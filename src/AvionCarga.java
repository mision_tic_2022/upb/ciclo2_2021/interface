public class AvionCarga implements Avion {
    private String color;
    private String placa;

    public AvionCarga(String color, String placa){
        this.color = color;
        this.placa = placa;
    }

    public void despegar(){
        System.out.println("despegando...");
    }

    public void aterrizar(){
        System.out.println("Aterrizando...");
    }
}
